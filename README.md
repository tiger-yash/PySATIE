# PySatie

PySatie is a Python module allowing for interactions with a [SATIE](https://gitlab.com/sat-metalab/SATIE) server.

In oprder to install PySatie locally, you will need [pyliblo](http://das.nasophon.de/pyliblo/), python wrappers to a [popular OSC library](http://liblo.sourceforge.net/)

You can install pyliblo easily
```
python3 -m pip install cython
python3 -m pip install pyliblo3
```

Then install PySatie with `python3 -m pip install .` or for an editable (development) mode: `python3 -m pip install -e .` (executed in the root of PySatie directory)

## Status

It's a work in progress. It's probably broken in some ways but that is intended to change.

## API documentation

`doc` directory contains Sphinx documentation. You can install Sphinx with `sudo apt-get install python3-sphinx python3-sphinx-rtd-theme`
Then in the `doc` directory: `make html` and you can view it locally by opening `./doc/build/html/index.html` with a browser of your choice.

Additionally you will need to enable typehints in documentation:

``` bash
python3 -m pip install sphinx-autodoc-typehints
```
