	SynthDef(\whitenoise, { |out, gate=1|
		var env = EnvGen.kr(Env.asr(0.03, 0.3, 0.01), gate, doneAction: Done.freeSelf);
		var sig = WhiteNoise.ar * env;
		Out.ar(out, sig)
	}).add;
