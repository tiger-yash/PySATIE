import json
import liblo
import logging
from os import path

from pysatie.config import OSCconfig
from pysatie.plugin import Plugin
from pysatie.property import Property

from typing import Any, Dict, List, TYPE_CHECKING

if TYPE_CHECKING:
    from pysatie.satie import Satie


PREFIX = "/satie"
SCENE_PREFIX = "/satie/scene"
RENDERER_PREFIX = "/satie/renderer"

logger = logging.getLogger(__name__)


class OSCserver(liblo.ServerThread):  # type: ignore
    def __init__(self, satie: 'Satie') -> None:
        logger.debug("OSCserver initing")
        self._satie: Satie = satie
        self._satie_address: liblo.Address = liblo.Address(OSCconfig.satie_host, OSCconfig.satie_port)
        self._scserver_address: liblo.Address = liblo.Address(OSCconfig.scserver_host, OSCconfig.scserver_port)
        logger.debug(f"Ready to start a liblo server thread with port {OSCconfig.port}")
        try:
            liblo.ServerThread.__init__(self, OSCconfig.port)
            logger.debug(f"liblo server thread started on port: {OSCconfig.port}")
        except liblo.ServerError as e:
            logger.error(f"Could not start liblo server thread on port {OSCconfig.port} because of {e}")
            raise liblo.ServerError from e

    # liblo causes the following error with type annotations:
    # Exception ignored in: 'liblo._msg_callback'
    # Traceback (most recent call last):
    #   File "/usr/lib/python3.6/inspect.py", line 1080, in getargspec
    #     raise ValueError("Function has keyword-only parameters or annotations"
    # ValueError: Function has keyword-only parameters or annotations, use getfullargspec() API which can support them
    @liblo.make_method("/satie/configuration", "s")
    def get_satie_configuration_json(self, path, payload):  # type: ignore
        """
        Handle reception of the JSON containing SATIE configuration
        """
        self._satie.emit_data('satie_query_result', satie_configuration=payload)

    @liblo.make_method("/plugins", "s")
    def parse_plugin_list(self, path, plugins):  # type: ignore
        """
        Parse the json object containing plugins list

        :param plugins: message containing raw JSON data
        :return:
        """
        plugins_raw = plugins[0]
        try:
            plugins_data = json.loads(plugins_raw)
        except json.JSONDecodeError as e:
            logger.error("Invalid plugin list json", e)
            return

        data_converted = self.convert_plugin_data(plugins_data)
        logger.debug(data_converted)
        self.remove_refreshed_plugins(data_converted)

        for datum in data_converted:
            if datum["name"] not in [p.name for p in self._satie.plugins]:
                self._satie.add_plugin(Plugin(self._satie, **datum))

        self._satie.emit('plugin_list_updated')

    def convert_plugin_data(self, payload: Dict) -> List[Dict]:
        """
        Convert the dictionary of SATIE plugins data obtained via JSON to entries convenient
        for Plugin class.

        :param payload: Dict The SATIE plugins JSON converted to dictionary via json.loads
        :return: List  A list of dictionaries suitable for instantiating Plugin classes
        """
        plugins = []
        for category, plugs in payload.items():
            for plugin, data in plugs.items():
                name = plugin
                description = data['description']
                plugin_type = data['type']

                plugins.append({
                    "name": name,
                    "satie_type": plugin_type,
                    "category": category,
                    "description": description
                })
        return plugins

    def remove_refreshed_plugins(self, data):
        new_names = [n["name"] for n in data]
        old_names = [n.name for n in self._satie.plugins]
        for name in old_names:
            if name not in new_names:
                self._satie.remove_plugin(self._satie.get_plugin_by_name(name))

    @liblo.make_method("/arguments", "s")
    def parse_plugin_properties(self, path, properties):  # type: ignore
        """
        Parse plugin properties[ 1, nil, tree ]

        :param properties: Message containing raw JSON data
        :return:
        """
        properties_raw = properties[0]
        try:
            props = json.loads(properties_raw)
        except Exception as e:
            logger.error(f"Invalid plugin properties json {e}")
            return

        for st, property_data in props.items():
            props = property_data['arguments']
            plugins = self._satie.get_plugins_by_type(property_data["srcName"])
            for plugin in plugins:
                plugin.properties = [
                    Property(
                        plugin,
                        name=prop['name'],
                        prop_type=prop['type'],
                        default_value=prop['value']
                    ) for prop in props
                ]

    # SATIE now sends /analysis/<node_name> <hasFreq> <envelope> <frequency> <midinote_number>
    # we need to implement pattern matching which doesn't yet work in liblo
    @liblo.make_method("/analysis", "sf")
    def get_envelope(self, path, args):  # type: ignore
        node = self._satie.nodes.get(args[0])
        if node:
            node.analysis = ('analysis', args[1])

    # SATIE now sends /trigger/<node_name> <value>
    # we need to implement pattern matching which doesn't yet work in liblo
    @liblo.make_method("/trigger", "sf")
    def get_trigger(self, path, args):  # type: ignore
        node = self._satie.nodes.get(args[0])
        if node:
            node.analysis = ('trigger', args[1])

    @liblo.make_method("/satie.heartbeat", None)
    def signal_heartbeat(self, path):  # type: ignore
        self._satie.emit('satie_heartbeat')

    @liblo.make_method("/satie/server/option", None)
    def get_server_option(self, path, vals):  # type: ignore
        """
        :param vals: should always be a list
        """
        data = {vals[0]: vals[1]}
        self._satie.emit_data('satie_query_result', **data)

    @liblo.make_method(None, None)
    def fallback(self, path, args):  # type: ignore
        logger.warning(f"received unhandled message {path} with arguments: {args}")

    def stop_server(self) -> None:
        self.free()
        logger.info("OSC server stopped")

    def load_synth_types(self) -> None:
        """
        Requests plugin/synths types

        :return:
        """
        self.send(path.join(PREFIX, "plugins"), 1)

    def send(self, address: liblo.Address, *args: Any, **kwargs) -> None:
        msg = liblo.Message(address)
        [msg.add(m) for m in args]
        if kwargs:
            for k, v in kwargs.items():
                msg.add(k)
                msg.add(v)
        liblo.send(self._satie_address, msg)

    ########################################
    # SC server level commands (bypass SATIE)

    def new_synth(self, synthDef: str, synthID: int, addAction: int = 0, targetID: int = 1, *args: Any):
        arguments = locals()
        msg = liblo.Message("/s_new")
        [msg.add(v) for k, v in arguments.items() if k not in ['self', 'args']]
        [msg.add(m) for m in args]
        liblo.send(self._scserver_address, msg)

    def set_synth_params(self, synthID: int, *args):
        msg = liblo.Message("/n_set")
        msg.add(synthID)
        [msg.add(m) for m in args]
        liblo.send(self._scserver_address, msg)

    ########################################
    # configuration and boot messages

    def satie_boot(self) -> None:
        """
        Boot SATIE
        """

        self.send(path.join(PREFIX, "boot"))

    def satie_quit(self) -> None:
        """
        Quit SATIE
        """

        self.send(path.join(PREFIX, "quit"))

    def satie_reboot(self) -> None:
        """
        Reboot SATIE, or boot it if not currently booted
        """

        self.send(path.join(PREFIX, "reboot"))

    def satie_configure(self, configuration: str) -> None:
        """
        Configure SATIE by sending it a stringified JSON configuration
        """

        self.send(path.join(PREFIX, "configure"), configuration)

    def get_satie_server_option(self, option: str) -> None:
        """
        Ask SATIE for the value of a server option in the current instance
        """
        self.send(path.join(PREFIX, "server", "options", "get"), option)

    def get_satie_configuration(self) -> None:
        """
        Request a SatieConfiguration JSON
        """
        self.send(path.join(PREFIX, "configuration", "get"))

    ########################################
    # SATIE level messages

    def load_sample(self, name: str, filepath: str) -> None:
        """
        Load a sample from file into a buffer and register it with SATIE.
        Currently limited to file formats handled by SuperCollider (.wav, .aif(f))

        :param name: unique name.
        :param filepath: absolute path to the soundfile.
        """
        self.send(path.join(PREFIX, "loadSample"), name, filepath)

    ########################################
    # scene messages

    def scene_clear(self) -> None:
        """
        Clear the SATIE scene

        :return:
        """

        self.send(path.join(SCENE_PREFIX, "clear"))

    def scene_set(self, key: str, value: Any) -> None:
        """
        Set a scene-wide keyword value
        """
        self.send(
            path.join(SCENE_PREFIX, "set"),
            key,
            value
        )

    ########################################
    # Scene level interaction

    def set_postprocessor_property_value(self, key: str, value: Any, postproc_type: str = "postproc") -> None:
        """
        Set a single property on a post-processor.

        :param key: name of the property
        :param val: property's value
        :param postproc_type: type of postproc, either ``postproc`` (default) or ``ambiposproc``

        A post-processor is a singleton containing a pipeline of synths.
        """

        if postproc_type == 'postproc' or postproc_type == 'ambipostproc':
            self.send(
                path.join(SCENE_PREFIX, postproc_type, "set"),
                key,
                value
            )
        else:
            logger.warning("Wrong value for postproc_type, should be postproc or ambiposproc")

    def set_postprocessor_property_vector(self, key: str, postproc_type: str = "postproc", *args: Any) -> None:
        """
        Set a post-processor property with a vector

        :param key: name of the property
        :param *args: a sequence of values
        :param postproc_type: type of postproc, either ``postproc`` (default) or ``ambiposproc``

        """

        if postproc_type == 'postproc' or postproc_type == 'ambipostproc':
            self.send(
                path.join(SCENE_PREFIX, postproc_type, "setarray"),
                key,
                *args
            )
        else:
            logger.warning("Wrong value for postproc_type, should be postproc or ambiposproc")

    def apply_postprocessor(self, postproc_type: str = "postproc") -> None:
        """
        Apply the configured post-processor

        """

        if postproc_type == 'postproc' or postproc_type == 'ambipostproc':
            self.send(
                path.join(SCENE_PREFIX, postproc_type, "apply"),
            )
        else:
            logger.warning("Wrong value for postproc_type, should be postproc or ambiposproc")

    def set_postprocessor_pipeline_property(self, name: str, *args: Any) -> None:
        """
        Set a property of the pipeline

        :param name: name of the postprocessor (project dependent)
        :param *args: key, value pairs for properties to affect
        """
        self.send(
            path.join(SCENE_PREFIX, "postproc", "prop", "set"),
            name,
            *args
        )

    def load_properties(self, plugin: str) -> None:
        """
        Send a request to SATIE for synth properties

        :param plugin: Plugin source name
        :return:
        """
        self.send(path.join(PREFIX, "plugindetails"), plugin)

    def set_responder_destination(self, ip: str, port: int) -> None:
        """
        Tell SATIE's inspector to send to this address:port

        :param ip: IP address
        :param port: port
        """
        self.send(path.join(PREFIX, "responder"), ip, port)

    def create_source_group(self, name: str) -> None:
        """
        Create an audio source group
        """
        self.send(path.join(SCENE_PREFIX, "createSourceGroup"), name)

    def create_effect_group(self, name: str) -> None:
        """
        Create an audio effect group
        """
        self.send(
            path.join(SCENE_PREFIX, "createEffectGroup"), name)

    def create_process_group(self, name: str) -> None:
        """
        Create an audio process group
        """
        self.send(
            path.join(SCENE_PREFIX, "createProcessGroup"), name)

    def create_source(self, node_id: str, source_name: str, group: str = 'default', **kwargs) -> None:
        """
        Instantiate an audio generator

        :param node_id: instance id, must be unique
        :param source_name: synthdef's name
        :param group: the name of the group the instance will be in
        """
        self.send(
            path.join(SCENE_PREFIX, "createSource"),
            node_id,
            source_name,
            group,
            **kwargs)

    def create_effect(self, node_id: str, effect_name: str, group: str = "defaultFX", auxbus: int = 0) -> None:
        """
        Instantiate an audio generator

        :param node_id: instance id, must be unique
        :param effect_name: synthdef's name
        :param group: the name of the group the instance will be in
        :param auxbus: index of the effects bus
        """
        self.send(
            path.join(SCENE_PREFIX, "createEffect"),
            node_id,
            effect_name,
            group,
            auxbus)

    def create_process(self, node_id: str, process_name: str, *args, **kwargs) -> None:
        """
        Create/instantiate a process

        :param node_id: instance id, must be unique
        :param process_name: name of the defined process
        :param *args: any arguments required to instantiate the process
        """
        self.send(
            path.join(SCENE_PREFIX, "createProcess"),
            node_id,
            process_name,
            *args,
            **kwargs
        )

    def delete_node(self, node_name: str) -> None:
        """
        Delete a node form the scene

        :param node_name:
        :return:
        """
        self.send(
            path.join(SCENE_PREFIX, "deleteNode"),
            node_name)

    def set_orientation(self, aziDeg: float, eleDeg: float) -> None:
        """
        Set renderer's orientation offset

        :params aziDeg: azimuth in degrees
        :params eleDeg: elevation in degrees
        """
        self.send(path.join(RENDERER_PREFIX, "setOrientationDeg"), aziDeg, eleDeg)

    def set_output_db(self, outputVolume: float) -> None:
        """
        Set renderer's output volume

        :params outputVolume: gain in dB (between -90 and +6)
        """
        self.send(path.join(RENDERER_PREFIX, "setOutputDB"),
            max(min(outputVolume, 6), -90))

    def set_dim(self, state: bool) -> None:
        """
        Set renderer's dim state

        :params state: True ou False
        """
        self.send(path.join(RENDERER_PREFIX, "setOutputDim"), int(state))

    def set_mute(self, state: bool) -> None:
        """
        Set renderer's output state

        :params state: True ou False
        """
        self.send(path.join(RENDERER_PREFIX, "setOutputMute"), int(state))

    def set_debug(self, debug: int) -> None:
        """
        Turn printing debug messages (in SuperCollider) on/off
        """
        self.send(path.join(SCENE_PREFIX, "debug"), debug)

    def enable_heartbeat(self, flag: bool) -> None:
        """
        Turn SATIE's status heartbeat on/off
        """
        self.send(path.join(PREFIX, "heartbeat"), int(flag))

    ########################################
    # Node messages

    def node_update(self,
                    node_type: str,
                    node_name: str,
                    azimuth: float,
                    elevation: float,
                    gain: float,
                    delay: float,
                    lp: float) -> None:
        """
        Update node properties

        :param node_type: source/effect or process
        :param node_name: named instance
        :param azimuth: horizontal angle degrees
        :param elevation: vertical angle degrees
        :param gain: decibels
        :param delay: milliseconds
        :param lp: low-pass filter frequency herz
        :param distance: meters
        """
        self.send(
            path.join(PREFIX, node_type, "update"),
            node_name,
            azimuth,
            elevation,
            gain,
            delay,
            lp,
        )

    def node_set(self, node_type: str, node_name: str, *args: Any) -> None:
        """
        Set properties

        :param node_type: either source, group or process
        :param node_name: instance name
        :param \*args: key, value pairs alternating
        """
        self.send(
            path.join(PREFIX, node_type, "set"),
            node_name,
            *args
        )

    def node_set_vector(self, node_type: str, node_name: str, key: str, *args: Any) -> None:
        """
        Set property

        :param node_type: either source, group or process
        :param node_name: instance name
        :param key: parameter name
        :param args:  sequence of values
        """
        self.send(
            path.join(PREFIX, node_type, "setvec"),
            node_name,
            key,
            *args
        )


    def node_state(self, node_type: str, node_name: str, state: int) -> None:
        """
        Set state (DSP computation)

        :param node_type: either source, group or process
        :param node_name: instance name
        :param state: (int) 1 = active, 0 = inactive
        """
        self.send(
            path.join(PREFIX, node_type, "state"),
            node_name,
            state
        )

    def node_event(self, node_type: str, node_name: str, event_name: str, *args: Any) -> None:
        """
        Execute an event associated with a node.

        :param node_type: either source, group or process
        :param node_name: instance name
        :param event_name: event name (usually a method defined by source, effect or process)
        :param \*args: optional parameters required by the event
        """
        self.send(
            path.join(PREFIX, node_type, "event"),
            node_name,
            event_name,
            *args
        )

    ########################################################################
    # Process messages

    def process_property(self, node_name: str, *args: Any) -> None:
        """
        Set a property of a process

        :param node_name: process instance name
        :param \*args: a sequence of key/value pairs
        """
        self.send(
            path.join(PREFIX, "process", "set"),
            node_name,
            *args
        )

    def process_eval(self, node_name: str, handler: str, *args: Any) -> None:
        """
        Evaluate a process method

        :param node_name: process instance name
        :param handler: name of the method to be evaluated
        :param args: any arguments required for the method
        """
        self.send(
            path.join(PREFIX, "process", "eval"),
            node_name,
            handler,
            *args
        )
