from pysatie.plugin import Plugin
from typing import Any, TYPE_CHECKING

if TYPE_CHECKING:
    from pysatie.plugin import Plugin


class Property:
    def __init__(self, plugin: Plugin, name: str, prop_type: str, default_value: Any) -> None:
        self._plugin = plugin
        self._name = name
        self._type = prop_type
        self._default = default_value

    @property
    def plugin(self) -> Plugin:
        return self._plugin

    @property
    def name(self) -> str:
        return self._name

    @name.setter
    def name(self, value: str) -> None:
        self._name = value

    @property
    def type(self) -> str:
        return self._type

    @type.setter
    def type(self, value: str) -> None:
        self._type = value

    @property
    def default_value(self) -> Any:
        return self._default

    @default_value.setter
    def default_value(self, value: Any) -> None:
        self._default = value
