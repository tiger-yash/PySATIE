"""
A Node represents an audio source or effect
"""
import logging

from pysatie import utils

from typing import Any, Dict, List, Optional, Tuple, TYPE_CHECKING

if TYPE_CHECKING:
    from pysatie.satie import Satie
    from pysatie.plugin import Plugin


logger = logging.getLogger(__name__)

class Node:
    def __init__(self, satie: 'Satie', id: str, plugin: 'Plugin', group: str = "", **kwargs) -> None:
        self._satie: 'Satie' = satie
        try:
            self._synth: 'Plugin' = plugin
            self._name: str = plugin.name
        except AttributeError:
            logger.exception("Expecting pysatie.satie.Plugin type")
            return
        self._id: str = id
        self._plugin: 'Plugin' = plugin
        self._group: str = group
        self._listener_position: Tuple[float, float, float] = (0.0, 0.0, 0.0)  # listener coordinates

        self._active: bool = False
        self._debug: bool = False

        self._azimuth: float = 0
        self._elevation: float = 0
        self._gain: float = -99
        self._delay: float = 1
        self._low_pass: float = 15000
        self._distance: float = 1
        self._doppler_factor: float = 100
        self._analysis: Optional[Dict] = {}

        self.set_default_group()
        if kwargs:
            plugin.synth_args = kwargs

    @property
    def name(self) -> str:
        return self._name

    @property
    def id(self) -> str:
        return self._id

    @property
    def group(self) -> str:
        return self._group

    @property
    def plugin(self) -> 'Plugin':
        return self._plugin

    @property
    def active(self) -> bool:
        return self._active

    @property
    def listener_position(self) -> Tuple[float, float, float]:
        return self._listener_position

    @listener_position.setter
    def listener_position(self, listener_pos: Tuple[float, float, float]) -> None:
        self._listener_position = listener_pos

    @property
    def analysis(self) -> Optional[Dict]:
        return self._analysis

    @analysis.setter
    def analysis(self, kv: List[float]) -> None:
        if len(kv) > 2:
            self._analysis[kv[0]] = kv[1:]

    def set_default_group(self) -> None:
        if not self._group:
            if self._plugin.category == 'generators':
                self._group = "default"
            if self._plugin.category == 'effects':
                self._group = "defaultFX"

    def initialize(self) -> None:
        """
        Instantiate the plugin. This method first figures what category this Plugin
        belongs to and then calls the appropriate method to create an instance.
        """
        if self._plugin.category == "generators":
            self._plugin.create_source_instance(self._id,  self._group)
        elif self._plugin.category == "effects":
            self._plugin.create_fx_instance(self._id, self._group, bus=0)
        else:
            logger.error("Plugin category {} is unsupported".format(self._plugin.category))

    def update(self, position: List[float]) -> None:
        """
        send update message

        :param position: object location [x,y,z]
        """
        self._azimuth, self._elevation, self._distance = utils.xyz_to_aed(position)
        self._gain = utils.gain_from_distance(self._distance)
        self._delay = utils.variable_delay(self._distance, self._doppler_factor)
        self._low_pass = utils.get_distance_freq(self._distance)

        # send update message
        if self._satie.osc:
            self._satie.osc.node_update(
                "source",
                self._id,
                self._azimuth,
                self._elevation,
                self._gain,
                self._delay,
                self._low_pass,
            )

        def update_raw(self, azi: float, ele: float, gain: float, delay: float, lpass: float) -> None:
            """
            send update message with raw values

            :param azi: degrees
            :param ele: degrees
            :param gain: dB
            :param delay: milliseconds
            :param lpass: low pass frequency (in Hz)
            """
            # send update message
            if self._satie.osc:
                self._satie.osc.node_update(
                    "source",
                    self._id,
                    azi,
                    ele,
                    gain,
                    delay,
                    lpass
                )

    def set(self, *args: Any) -> None:
        """
        set attribute(s) on the node

        :param \*args: key/value pairs. Invalid keys will be ignored by SATIE
        """
        if self._satie.osc:
            self._satie.osc.node_set(
                "source",
                self._id,
                *args
            )
