import logging
import psutil
import subprocess
import sys

from threading import Thread

import pysatie.config as config


logger = logging.getLogger(__name__)


class SclangProcess:
    """
    Represents an external ``sclang`` process. It basically allows for
    starting and stopping the process. Presently the workflow is very
    simple: we start an ``sclang`` process if it snot already running,
    otherwise we do nothing and assume that the the running ``sclang``
    contains SATIE (and if it doesn't, nothing will happen.)
    """

    def __init__(self) -> None:
        """Constructor: it only figures out what plaform we are on"""
        self._sc_thread: Thread = None
        self._process = None
        if sys.platform.startswith("linux"):
            self._platform = config.LinuxPlatform()
        if sys.platform.startswith("darwin"):
            self._platform = config.DarwinPlatform()

    def run(self) -> None:
        """
        Start an external ``sclang`` process if one isn't already running.
        """
        if not self.sclang_is_running():
            logger.debug(f"The configuration path is: {self._platform.satie_config_path}")

            def target(*args, **kwargs):
                self._process = subprocess.Popen(
                    [self._platform.SCLANG_CMD, self._platform.satie_config_path]
                )
                self._process.communicate()

            self._sc_thread = Thread(target=target)
            self._sc_thread.start()

    def stop(self) -> None:
        """Stop the running subprocess"""
        self._process.kill()

    def sclang_is_running(self) -> bool:
        """Check if ``sclang`` is already running"""
        for proc in psutil.process_iter():
            if self._platform.SCLANG_CMD in proc.name():
                logger.info(
                    f"{self._platform.SCLANG_CMD} is already running\n\
                    Not starting a new process."
                )
                return True
        return False
